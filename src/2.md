<link rel="stylesheet" href="/assets/css/styles.css"></link>
<script src="/assets/js/collapse_lists.js"></script>


# Bloques de código

Podemos ver algunos bloques de diferentes lenguajes de programación.

-   En python
    
    ```python
    
        :::python3
        class Elephant:
            """Elephants that sort themselves by trunk size"""
            def __init__(self, name, trunk_size):
                self.name = name
                self.trunk_size = trunk_size
        
            def __repr__(self):
                return f"<{self.name} [trunk {self.trunk_size}]>"
        
            def __gt__(self, other):
                return self.trunk_size < other.trunk_size
        
        elephants = [
            Elephant('mama', 5),
            Elephant('baby', 1),
            Elephant('grandma', 7),
            Elephant('daddy', 6),
            Elephant('son', 3),
        ]
        
        from pprint import pprint
        
        pprint(elephants)
        
        print("\nAnd now the biggest elephants go first:")
        elephants.sort()
        
        pprint(elephants)
    
    ```
-   En javascript
    
    ```js
    
        var m = new Map();
        m.set(1, "black");
        m.set(2, "red");
        
        for (var n of m) {
            console.log(n);
        }
        
        // Output:
        // 1,black
        // 2,red
    
    ```


# Diagramas de guitarra

-   Tarjetas
    
      <div class="cards">
      <div class="card">
      <span>Estado fundamental - Set 4</span><br><br>
    e|-<b class="Q">1</b>-|---|---|---|---|---|<br>
    B|---|---|-<b class="T">3</b>-|---|---|---|<br>
    G|---|---|-<b class="F">2</b>-|---|---|---|<br>
    D|---|---|---|---|---|---|<br>
    A|---|---|---|---|---|---|<br>
    E|---|---|---|---|---|---|
      </div>
      <div class="card">TWO</div>
      <div class="card">THREE</div>
      <div class="card">FOUR</div>
      <div class="card">FIVE</div>
      <div class="card">SIX</div>
      <div class="card">SEVEN</div>
      <div class="card">EIGHT</div>
      <div class="card">NINE</div>
      <div class="card">TEN</div>
      <div class="card">ELEVEN</div>
      <div class="card">TWELVE</div>
    </div>

<script>
var listas = document.getElementsByTagName("ul");
for (var i = 0; i < listas.length; i+=1){
  CollapsibleLists.applyTo(listas[i], true);
}
</script>

